#import <Foundation/Foundation.h>
#import "SimpleThread.h"

int main (int argc, const char * argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSLog(@"Starting ThreadExample...\n");
	SimpleThread *st = [[SimpleThread alloc] init];
	[st startRunning];
	
	//[NSThread sleepForTimeInterval:5];
	[st waitForFinish];
    [pool drain];
    return 0;
}
