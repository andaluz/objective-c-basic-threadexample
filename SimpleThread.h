//
//  SimpleThread.h
//  ThreadExample
//
//  Created by Nordin on 24-10-13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//



@interface SimpleThread : NSObject {
	NSThread *thread;
	NSConditionLock *lock;
}


- (void) startRunning;
- (void) waitForFinish;

@end
