//
//  SimpleThread.m
//  ThreadExample
//
//  Created by Nordin on 24-10-13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "SimpleThread.h"

#define THREAD_STARTED 0
#define THREAD_FINISHED 1

@implementation SimpleThread {
	
}


- (void) run: (id) object{
	[lock lock];
	NSLog(@"Another thread says: run\n");
	
	[lock unlockWithCondition:THREAD_FINISHED];
}
	

- (void) startRunning {
	NSLog(@"startRunning\n");
	
	lock = [[NSConditionLock alloc] initWithCondition: THREAD_STARTED];
	if(thread == nil) {
		thread = [[NSThread alloc] initWithTarget:self
								selector:@selector(run:)
								  object:nil];
	}
	
	// Another way to spawn a new thread
	//[NSThread detachNewThreadSelector:@selector(run:) toTarget:self withObject:nil];
	
	[thread start];
}

- (void) waitForFinish {
	//if(thread != nil)
	//	[thread j
	[lock lockWhenCondition:THREAD_FINISHED];
}

@end
